//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ulaznicar.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Izvodjac
    {
        public Izvodjac()
        {
            this.Koncert = new HashSet<Koncert>();
        }
    
        public int Id { get; set; }
        public string naziv { get; set; }
    
        public virtual ICollection<Koncert> Koncert { get; set; }
    }
}
